# 1.0
## Features
- you can select the encoding of the opened file
- fb2 files
- zip archives with a text file or with fb2

# 0.8
## Features
- other languages supported. There is only ru-files now. Language depends on system region settings.
 
# 0.7
## TODO
- add languages
- read files with different encodings (there is only windows-1251 now)
- read fb2
- chnage line spacing
