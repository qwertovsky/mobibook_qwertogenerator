package com.qwertovsky.mobibook.generator;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Enumeration;
import java.util.Scanner;
import java.util.Vector;
import java.util.jar.Attributes;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Scene;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;

import javax.imageio.ImageIO;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class Generator
{
	private Profile profile;
	private int sysline = 0;
	private String bookName;
	private javafx.scene.text.Font fxFont;
	private javafx.scene.paint.Color fontFXColor;
	private javafx.scene.paint.Color backgroundFXColor;
	private Font awtFont;
	private Color fontAWTColor;
	private Color backgroundAWTColor;

	private short chunkCount = 1;
	private int bookSize = 0;
	private int chunkSize = 0x7FFF;
	private File tempDir;
	private File saveDirFile;

	public Generator(javafx.scene.text.Font fxFont
			, javafx.scene.paint.Color fontColor
			, javafx.scene.paint.Color backgroundColor
			, Profile profile
			, int sysline
			, File saveDirFile)
	{
		this.profile = profile;
		this.fxFont = fxFont;
		this.fontFXColor = fontColor;
		this.backgroundFXColor = backgroundColor;
		this.sysline = sysline;
		this.saveDirFile = saveDirFile;
		
		tempDir = new File("temp");
		tempDir.mkdir();
	}
	
	// ---------------------------------------------
	public Generator(Font awtFont
			, Color fontColor
			, Color backgroundColor
			, Profile profile
			, int sysline
			, File saveDirFile)
	{
		this.profile = profile;
		this.awtFont = awtFont;
		this.fontAWTColor = fontColor;
		this.backgroundAWTColor = backgroundColor;
		this.sysline = sysline;
		this.saveDirFile = saveDirFile;
		
		tempDir = new File("temp");
		tempDir.mkdir();
	}

	// ---------------------------------------------
	/**
	 * Create midlet
	 * @param file text book file
	 * @param bookName book name 
	 */
	public void create(File file, String bookName, String charsetName) throws Exception
	{
		//transliteration of book name
		this.bookName = toTranslit(bookName);
		
		if(fxFont != null)
		{
			if(Platform.isFxApplicationThread())
			{
				generateFXFont();
			}
			else
			{
				awtFont = new Font(fxFont.getName(),0, (int) fxFont.getSize());
				fontAWTColor = new Color((float)fontFXColor.getRed()
						, (float)fontFXColor.getGreen()
						, (float)fontFXColor.getBlue());
				backgroundAWTColor = new Color((float)backgroundFXColor.getRed()
						, (float)backgroundFXColor.getGreen()
						, (float)backgroundFXColor.getBlue());
				generateAWTFont();
			}
			
		}
		if(awtFont != null)
			generateAWTFont();
		generateText(file, charsetName);
		generateConfig();
		archive();
		cleanTemp(tempDir);
	}
	
	// ---------------------------------------------
	private String toTranslit(String text)
	{
		char[] textArray = text.toLowerCase().toCharArray();
		String alpha = new String("абвгдеёжзийклмнопрстуфхцчшщъыьэюя");
		String[] t = new String[]{"a","b","v","g","d","e","yo","zh","z","i","y","k","l","m","n","o","p",
		        "r","s","t","u","f","h","c","ch","sh","shch","","y","","e","yu","ya"};
		StringBuffer buffer = new StringBuffer();
		
		for(int i = 0; i < textArray.length; i++)
		{
			char c = textArray[i];
			char nextC = i + 1 == textArray.length ? 0:textArray[i+1];
			int alphaIndex = alpha.indexOf(c);
			if(alphaIndex == -1)
			{
				buffer.append(c);
				continue;
			}
			String transC = t[alphaIndex];
			
			if(text.charAt(i) == c) // lower case
				buffer.append(transC);
			else //upper case
			{
				if(nextC != 0
						&& text.charAt(i + 1) == nextC
						&& transC.length() > 1) // next in lower case
				{
					buffer.append(transC.substring(0,1).toUpperCase());
					buffer.append(transC.substring(1));
				}
				else
					buffer.append(transC.toUpperCase());
			}
		}
		return buffer.toString();
	}

	// ---------------------------------------------
	private void cleanTemp(File file)
	{
		if (file.isDirectory())
		{
			for (File nestedFile : file.listFiles())
			{
				cleanTemp(nestedFile);
			}
		}

		file.delete();
	}

	// ---------------------------------------------
	private static String getLines(FontMetrics fm
			, Vector<String> parts
			, double screenWidth)
	{
		StringBuilder sb = new StringBuilder();

		//first line has indent
		String line = " ";
		for (String part : parts)
		{
			//get line width with new part
			int imgLength = 0;
			if (!part.endsWith(" ") && !part.endsWith("\n"))
				imgLength = fm.stringWidth(line + part + "-");
			else
				imgLength = fm.stringWidth(line + part.trim());
			//if not fit to screen
			if (imgLength + 2 > screenWidth)
			{
				//if end of word or line
				if (line.endsWith(" ") || line.endsWith("\n"))
					line = line.trim();
				//should be hyphenation
				else
					line += "-";
				//start new line
				sb.append(line + "\n");
				line = "";
			}
			line += part;
		}
		sb.append(line + "\n");
		return sb.toString();
	}
	
	// --------------------------------------------
	private static String getLines(javafx.scene.text.Font font
			, Vector<String> parts
			, double screenWidth)
	{
		StringBuilder sb = new StringBuilder();

		String line = " ";
		for (String part : parts)
		{
			double imgLength = 0;
			String testLine;
			if (!part.endsWith(" ") && !part.endsWith("\n"))
				testLine = line + part + "-";
			else
				testLine = line + part.trim();
			//get string width
			Text text = new Text(testLine);
			text.setFont(font);
			imgLength = text.getLayoutBounds().getWidth();
			
			if (imgLength + 2 > screenWidth)
			{
				if (line.endsWith(" ") || line.endsWith("\n"))
					line = line.trim();
				else
					line += "-";
				sb.append(line + "\n");
				line = "";
			}
			line += part;
		}
		sb.append(line + "\n");
		return sb.toString();
	}
	
	// --------------------------------------------
	private static InputStreamReader getFileTextStream(File file, String charsetName)
	throws Exception
	{
		InputStreamReader textReader = null;
		if(file == null)
			throw new Exception("Input file is not defined");
		InputStream fileIS = new FileInputStream(file);
		if(file.getName().endsWith(".fb2"))
		{
			textReader = getFB2Reader(fileIS, charsetName);
		}
		else if(file.getName().endsWith(".zip"))
		{
			ZipFile zip = new ZipFile(file);
			Enumeration<? extends ZipEntry> entries = zip.entries();
			while(entries.hasMoreElements())
			{
				ZipEntry entry = entries.nextElement();
				if(entry.getName().endsWith(".fb2"))
				{
					InputStream  xmlIS = zip.getInputStream(entry);
					textReader = getFB2Reader(xmlIS, charsetName);
					return textReader;
				}
				if(entry.getName().endsWith(".txt"))
				{
					InputStream  textIS = zip.getInputStream(entry);
					textReader = new InputStreamReader(textIS, charsetName);
					return textReader;
				}
			}
			throw new Exception("Input text file is not fount in archive");
		}
		else
		{
			textReader = new InputStreamReader(fileIS, charsetName);
		}
		return textReader;
	}
	
	// --------------------------------------------
	private static InputStreamReader getFB2Reader(InputStream xmlIS, String charsetName)
	throws UnsupportedEncodingException, TransformerException
	{
		Reader xmlReader = new InputStreamReader(xmlIS, charsetName);
		StreamSource xmlSource = new StreamSource(xmlReader);
		
		StreamSource styleSource = new StreamSource(new File("resources/FB2_2_txt.xsl"));
		TransformerFactory factory = TransformerFactory.newInstance();
	    Transformer transformer = factory.newTransformer(styleSource);
	    
	    ByteArrayOutputStream bos = new ByteArrayOutputStream();
	    StreamResult textResult = new StreamResult(bos);
	    transformer.transform(xmlSource, textResult);
	    InputStreamReader textReader = new InputStreamReader(new ByteArrayInputStream(bos.toByteArray()));
	    return textReader;
	}

	// --------------------------------------------
	public static byte[] getText(File textFile
			, String charsetName
			, Font font
			, double screenWidth
			, int size)
			throws UnsupportedEncodingException
			, FileNotFoundException
			, IOException
			, TransformerException
			, Exception
	{
		Graphics2D g2d = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB)
				.createGraphics();
		g2d.setFont(font);
		FontMetrics fm = g2d.getFontMetrics();

		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		InputStreamReader textStream = getFileTextStream(textFile, charsetName);
		
		Scanner scanner = new Scanner(textStream);
		while (scanner.hasNext())
		{
			String line = scanner.nextLine();
			if (line.matches("Глава [\\d]*") || line.matches("Chapter [\\d]*"))
				bos.write('\n');
			bos.write(' ');
			Vector<String> syllables = Hyphenator.hyphenateWord(line);
			String str = getLines(fm, syllables, screenWidth);
			bos.write(str.getBytes("windows-1251"));
			if(size != 0 && bos.size() >= size)
				break;
		}

		bos.flush();
		byte[] textBytes = bos.toByteArray();
		bos.close();
		return textBytes;
	}

	// --------------------------------------------
	public static byte[] getText(File textFile
			, String charsetName
			, javafx.scene.text.Font font
			, double screenWidth
			, int size)
			throws UnsupportedEncodingException
			, FileNotFoundException
			, IOException
			, TransformerException
			, Exception
	{
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		InputStreamReader textStream = getFileTextStream(textFile, charsetName);

		Scanner scanner = new Scanner(textStream);
		while (scanner.hasNext())
		{
			String line = scanner.nextLine();
			if (line.matches("Глава [\\d]*") || line.matches("Chapter [\\d]*"))
				bos.write('\n');
			bos.write(' ');
			Vector<String> syllables = Hyphenator.hyphenateWord(line);
			String str = getLines(font, syllables, screenWidth);
			bos.write(str.getBytes("windows-1251"));
			if(size != 0 && bos.size() >= size)
				break;
		}

		bos.flush();
		byte[] textBytes = bos.toByteArray();
		bos.close();
		return textBytes;
	}

	// --------------------------------------------
	private void generateText(File textFile, String charsetName)
	throws UnsupportedEncodingException, IOException
	, TransformerException
	, Exception
	{
		byte[] textBytes = null;
		if(awtFont != null)
			textBytes = getText(textFile, charsetName, awtFont, profile.getWidth(), 0);
		if(fxFont != null)
			textBytes = getText(textFile, charsetName, fxFont, profile.getWidth(), 0);

		// write to chunks
		bookSize = textBytes.length;
		chunkCount = (short) (bookSize / chunkSize + 1);

		File textDir = new File(tempDir.getPath() + File.separator + "text");
		textDir.mkdir();
		for (int i = 0; i < chunkCount; i++)
		{
			int off = i * chunkSize;
			int len = chunkSize;
			if (off + len > bookSize)
				len = bookSize - off;
			File chunkFile = new File(textDir.getPath() + File.separator
					+ "chunk." + i);
			FileOutputStream os = new FileOutputStream(chunkFile);
			os.write(textBytes, off, len);
			os.flush();
			os.close();
		}

	}

	// --------------------------------------------
	private void generateAWTFont() throws IOException
	{
		byte[] ascii = new byte[256 - ' '];
		for (int i = ' ', j = 0; i <= 255; i++, j++)
		{
			ascii[j] = (byte) i;
		}
		String s = new String(ascii, "windows-1251");

		int fontSize = awtFont.getSize();
		Graphics2D g2d = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB)
				.createGraphics();

		g2d.setFont(awtFont);
		FontMetrics fm = g2d.getFontMetrics();
		int width = fm.stringWidth(s);
		BufferedImage img2 = new BufferedImage(width, fontSize + 2,
				BufferedImage.TYPE_INT_ARGB);
		g2d = img2.createGraphics();
		g2d.setPaint(backgroundAWTColor);
		g2d.fillRect(0, 0, width, fontSize + 2);
		g2d.setPaint(fontAWTColor);
		g2d.setFont(awtFont);

		int x = 0;
		int y = fontSize - 1;
		g2d.drawString(s, x, y);

		// write
		File fontDir = new File(tempDir.getPath() + File.separator + "fonts");
		fontDir.mkdir();

		// write default.props
		DataOutputStream fontProps = new DataOutputStream(new FileOutputStream(
				fontDir.getPath() + File.separator + "default.props"));
		ByteBuffer buffer = ByteBuffer.allocate(256 * 2 + 2);
		buffer.order(ByteOrder.LITTLE_ENDIAN);
		buffer.putShort((short) 256);
		for (int i = 0; i < 256; i++)
		{
			if (i < ' ')
			{
				buffer.putShort((short) 0);
				continue;
			}
			buffer.putShort((short) fm.stringWidth(s.substring(0, i - ' ')));
		}
		fontProps.write(buffer.array());
		fontProps.flush();
		fontProps.close();

		g2d.dispose();

		// write default.png
		File imageFile = new File(fontDir.getPath() + File.separator
				+ "default.png");
		ImageIO.write(img2, "png", imageFile);
	}
	
	// --------------------------------------------
	private void generateFXFont() throws IOException
	{
		byte[] ascii = new byte[256 - ' '];
		for (int i = ' ', j = 0; i <= 255; i++, j++)
		{
			ascii[j] = (byte) i;
		}
		String s = new String(ascii, "windows-1251");
		
		Text text = new Text(s);
		text.setFont(fxFont);
		text.setFill(fontFXColor);
		StackPane p = new StackPane();
		p.getChildren().add(text);
		
		Scene scene = new Scene(p, backgroundFXColor);
		WritableImage wi = scene.snapshot(null);
		
		// write
		File fontDir = new File(tempDir.getPath() + File.separator + "fonts");
		fontDir.mkdir();
		// write default.png
		File imageFile = new File(fontDir.getPath() + File.separator
				+ "default.png");
		ImageIO.write(SwingFXUtils.fromFXImage(wi, null), "png", imageFile);
		
		// write default.props
		DataOutputStream fontProps = new DataOutputStream(new FileOutputStream(
				fontDir.getPath() + File.separator + "default.props"));
		ByteBuffer buffer = ByteBuffer.allocate(256 * 2 + 2);
		buffer.order(ByteOrder.LITTLE_ENDIAN);
		buffer.putShort((short) 256);
		for (int i = 0; i < 256; i++)
		{
			if (i < ' ')
			{
				buffer.putShort((short) 0);
				continue;
			}
			
			text.setText(s.substring(0, i - ' '));
			short charPosition = (short)text.getLayoutBounds().getWidth();
			buffer.putShort(charPosition);
		}
		fontProps.write(buffer.array());
		fontProps.flush();
		fontProps.close();
	}

	// --------------------------------------------
	private void generateConfig() throws Exception
	{
		DataOutputStream cfg = new DataOutputStream(new FileOutputStream(
				tempDir.getPath() + File.separator + "config"));
		ByteBuffer buffer = ByteBuffer.allocate(27);
		buffer.order(ByteOrder.LITTLE_ENDIAN);

		buffer.putInt(bookSize);
		buffer.putShort((short) 0);
		buffer.put((byte) 0);
		buffer.putShort((short)profile.getWidth());
		buffer.putShort((short)profile.getHeight());
		buffer.put((byte) 1);
		buffer.put((byte) profile.getlSoft());
		buffer.put((byte) profile.getrSoft());
		buffer.put((byte) profile.getMenu());
		buffer.put((byte) profile.getOnUp());
		buffer.put((byte) profile.getOnDown());
		buffer.put((byte) profile.getOnLeft());
		buffer.put((byte) profile.getOnRight());
		buffer.putShort((short) chunkCount);
		buffer.putInt(chunkSize);
		buffer.put((byte) sysline);
		buffer.put((byte) 6);

		cfg.write(buffer.array());
		cfg.flush();
		cfg.close();

	}

	// --------------------------------------------
	private void archive() throws IOException
	{
		// create manifest
		File manifestDir = new File(tempDir.getPath() + File.separator
				+ "META-INF");
		manifestDir.mkdir();
		// open manifest
		JarFile sourceJar = new JarFile("bookgen.dat");
		Manifest manifest = sourceJar.getManifest();
		Attributes attr = manifest.getMainAttributes();
		// add properties to main attributes
		attr.putValue("MIDlet-Name", bookName);
		attr.putValue("MIDlet-1", bookName + ", /book.png, MidpBook");
		OutputStream os = new FileOutputStream(tempDir.getPath()
				+ File.separator + "META-INF" + File.separator + "MANIFEST.MF");
		manifest.write(os);
		os.close();

		// write to jar
		File distJarFile = new File(saveDirFile.getAbsolutePath() + File.separator + bookName + ".jar");
		OutputStream fos = new FileOutputStream(distJarFile);
		JarOutputStream jos = new JarOutputStream(fos, manifest);
		// copy entries
		Enumeration<JarEntry> entries = sourceJar.entries();
		while (entries.hasMoreElements())
		{
			JarEntry sourceEntry = entries.nextElement();
			InputStream entryIS = sourceJar.getInputStream(sourceEntry);
			if (sourceEntry.getName().equals("META-INF/MANIFEST.MF"))
				continue;

			JarEntry distEntry = new JarEntry(sourceEntry.getName());
			jos.putNextEntry(distEntry);
			byte[] buffer = new byte[4096];
			int bytesRead = 0;
			while ((bytesRead = entryIS.read(buffer)) != -1)
			{
				jos.write(buffer, 0, bytesRead);
			}
			entryIS.close();
			jos.flush();
			jos.closeEntry();
		}
		// add text, font, config
		File fontDir = new File(tempDir + File.separator + "fonts");
		File textDir = new File(tempDir + File.separator + "text");
		File configFile = new File(tempDir + File.separator + "config");
		File baseDir = new File("temp");
		addFileToJar(fontDir, baseDir, jos);
		addFileToJar(textDir, baseDir, jos);
		addFileToJar(configFile, baseDir, jos);

		jos.close();
		fos.close();

		// write to jad

		File jadFile = new File(saveDirFile.getAbsolutePath() + File.separator + bookName + ".jad");
		OutputStream jadOS = new FileOutputStream(jadFile);
		try
		{
			for (Object key : attr.keySet())
			{
				String str = key.toString() + ": "
						+ attr.getValue(key.toString()) + "\n";
				jadOS.write(str.getBytes());
			}

			jadOS.write(("MIDlet-Jar-URL: " + bookName + ".jar\n").getBytes());
			jadOS.write(("MIDlet-Jar-Size: " + distJarFile.length() + "\n")
					.getBytes());
		} finally
		{
			jadOS.close();
		}
	}

	// --------------------------------------------
	private void addFileToJar(File source, File baseDir, JarOutputStream jos)
			throws IOException
	{
		if (source.isDirectory())
		{
			String entryName = source.getPath();
			entryName = entryName.replaceFirst(baseDir.getPath(), "");
			entryName = entryName.replace("\\", "/");
			if (!entryName.endsWith("/"))
				entryName += "/";

			JarEntry distEntry = new JarEntry(entryName);
			distEntry.setTime(source.lastModified());
			jos.putNextEntry(distEntry);
			jos.closeEntry();

			for (File nestedFile : source.listFiles())
			{
				addFileToJar(nestedFile, baseDir, jos);
			}
		} else
		{
			String entryName = source.getPath();
			entryName = entryName.replaceFirst(baseDir.getPath(), "");
			entryName = entryName.replace("\\", "/");
			if (entryName.startsWith("/"))
				entryName = entryName.substring(1);

			JarEntry distEntry = new JarEntry(entryName);
			distEntry.setTime(source.lastModified());
			jos.putNextEntry(distEntry);
			InputStream is = new FileInputStream(source);
			try
			{
				byte[] buffer = new byte[4096];
				int bytesRead = 0;
				while ((bytesRead = is.read(buffer)) != -1)
				{
					jos.write(buffer, 0, bytesRead);
				}
				jos.flush();
				jos.closeEntry();
			} finally
			{
				is.close();
			}
		}
	}
}
