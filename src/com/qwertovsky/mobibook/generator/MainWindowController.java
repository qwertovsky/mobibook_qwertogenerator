package com.qwertovsky.mobibook.generator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;

import org.ini4j.Ini;
import org.ini4j.InvalidFileFormatException;
import org.ini4j.Profile.Section;

public class MainWindowController implements Initializable
{
	ObservableList<Profile> profilesList;
	FXMLLoader profilesLoader;
	Scene profilesScene;
	Window window;
	Stage profilesStage;
	ResourceBundle profilesBundle;
	ResourceBundle mainBundle;
	private File selectedFile;
	private Font font;
	private Previewer previewer;
	MidletTask task;
	
	//options
	Ini configIni;
	private int sysline = 0;
	private String openDir;
	private String saveDir;
	private String profileName;
	private String fontName;
	private String charsetName = "windows-1251";
	private int fontSize;
	private String textColor = "#777777";
	private String bgColor = "#000000";
	
	@FXML
	private ChoiceBox<Profile> profilesCB;
	@FXML
	private ProfilesController profilesController;
	@FXML
	private ChoiceBox<String> charsetNameCB;
	@FXML
	private TextField textName;
	@FXML
	private TextField fileName;
	@FXML
	private Label versionL;
	@FXML
	private Label fontL;
	@FXML
	private Label exampleL;
	@FXML
	private ScrollPane previewSP;
	@FXML
	private ColorPicker backgroundCP;
	@FXML
	private ColorPicker textCP;
	@FXML
	private ChoiceBox<String> syslineCB;
	@FXML
	private Region piShadow;
	@FXML
	private ProgressIndicator progressIndicator;

	public MainWindowController()
	{
		profilesList = FXCollections.observableArrayList();
		
		File configFile = new File("config.ini");
		
		//get config options
		try
		{
			if(!configFile.exists())
			{
				new MessageBox(window, mainBundle.getString("errorWindowTitle"),
						mainBundle.getString("errorConfigFileNotExists"));
				configFile.createNewFile();
			}
			configIni = new Ini(configFile);
			Section defaultSection = configIni.get("default");
			sysline = defaultSection.get("sysline", Integer.class);
			openDir = defaultSection.get("openDir", String.class);
			saveDir = defaultSection.get("saveDir", String.class);
			profileName = defaultSection.get("profile", String.class);
			fontName = defaultSection.get("fontName", String.class);
			fontSize = defaultSection.get("fontSize", Integer.class);
			textColor = defaultSection.get("textColor", String.class);
			bgColor = defaultSection.get("bgColor", String.class);
		} catch (InvalidFileFormatException e)
		{
			new MessageBox(window, mainBundle.getString("errorWindowTitle"),
					mainBundle.getString("errorFileFormat"));
		} catch (IOException e)
		{
			new MessageBox(window, mainBundle.getString("errorWindowTitle")
					, mainBundle.getString("errorFileRead") + "\n" + e.getMessage());
		} catch (Exception e)
		{
			// nothing
		}
		
		
		if(fontSize == 0)
			fontSize = 11;
		font = new Font(fontName, fontSize);
		
		// fill profiles list
		try
		{
			URL fxml = new File("resources/ProfilesWindow.xml").toURI().toURL();
			profilesLoader = new FXMLLoader(fxml);
			try
			{
				profilesBundle = ResourceBundle.getBundle("ProfilesWindow");
			} catch(Exception e)
			{
				Locale locale = new Locale("ru","RU");
				profilesBundle = ResourceBundle.getBundle("ProfilesWindow", locale);
			}
			profilesLoader.setResources(profilesBundle);
			profilesScene = (Scene) profilesLoader.load();
			profilesController = profilesLoader.getController();
		} catch (Exception e)
		{
			String error = mainBundle.getString("errorProfilesInitialize") + ".\n"
				+ e.getMessage();
			new MessageBox(window, mainBundle.getString("errorWindowTitle"), error);
		}
		profilesList.addAll(profilesController.getProfilesList());
		
		selectedFile = new File("sample");
		previewer = new Previewer();
		previewer.setFont(font);
	}
	
	
	@SuppressWarnings("unused")
	@FXML
	private void showProfiles(ActionEvent event)
	{
		if (profilesStage == null)
		{
			profilesStage = new Stage();
			profilesStage.initStyle(StageStyle.UTILITY);
			profilesStage.initModality(Modality.APPLICATION_MODAL);
			profilesStage.setScene(profilesScene);
			profilesController.window = profilesStage;
			if(profilesBundle != null)
				profilesStage.setTitle(profilesBundle.getString("stageTitle"));
		}
		profilesStage.showAndWait();
	}
	
	//--------------------------------------------
	@SuppressWarnings("unused")
	@FXML
	private void showAboutWindow(ActionEvent event)
	{
		Stage aboutStage = new Stage();
		aboutStage.initStyle(StageStyle.UTILITY);
		aboutStage.initModality(Modality.APPLICATION_MODAL);
		aboutStage.initOwner(window);
		
		Scene scene;
		try
		{
			FXMLLoader aboutLoader = new FXMLLoader(new File("resources/AboutWindow.xml").toURI().toURL());
			ResourceBundle aboutBundle = null;
			try
			{
				aboutBundle = ResourceBundle.getBundle("AboutWindow");
			} catch(Exception e)
			{
				Locale locale = new Locale("ru","RU");
				aboutBundle = ResourceBundle.getBundle("AboutWindow", locale);
			}
			aboutLoader.setResources(aboutBundle);
			scene = (Scene) aboutLoader.load();
			aboutStage.setScene(scene);
			aboutStage.setTitle(aboutBundle.getString("windowTitle"));
			if(window != null)
			{
				aboutStage.setX(window.getX()+(window.getWidth()-scene.getWidth()) / 2);
				aboutStage.setY(window.getY() + (window.getHeight()-scene.getHeight()) / 2);
			}
			aboutStage.showAndWait();
		} catch (Exception e)
		{
			new MessageBox(window, mainBundle.getString("errorWindowTitle")
					, e.getMessage());
		}
		
	}
	
	//--------------------------------------------
	@SuppressWarnings("unused")
	@FXML
	private void showFileOpenDialog()
	{
		FileChooser fileChooser = new FileChooser();
		ExtensionFilter supportFilter = new ExtensionFilter("All supported file formats"
				, "*.txt", "*.fb2", "*.zip");
		ExtensionFilter txtFilter = new ExtensionFilter("Plain text document (*.txt)", "*.txt");
		ExtensionFilter fb2Filter = new ExtensionFilter("Fiction book (*.fb2)", "*.fb2");
		ExtensionFilter zipFilter = new ExtensionFilter("Zip archive with TXT or FB2 (*.zip)", "*.zip");
		ExtensionFilter allFilter = new ExtensionFilter("All (*.*)", "*");
		fileChooser.getExtensionFilters()
			.addAll(supportFilter, txtFilter, fb2Filter, zipFilter, allFilter);
		File openDirFile = new File(".");
		if(openDir != null && openDir.length() > 0)
		{
			openDirFile = new File(openDir);
		}
		fileChooser.setInitialDirectory(openDirFile);

		try
		{
			selectedFile = fileChooser.showOpenDialog(window);
		} catch(IllegalArgumentException e)
		{
			openDir = null;
			fileChooser.setInitialDirectory(new File("."));
			selectedFile = fileChooser.showOpenDialog(window);
		}
		if(selectedFile == null)
			return;
		String selectedFileName = selectedFile.getName();
		String selectedFilePath = selectedFile.getAbsoluteFile().getPath();
		openDir = selectedFile.getParent();
		fileName.setText(selectedFilePath);
		textName.setText(selectedFileName);
		updatePreviewText();
	}
	
	//--------------------------------------------
	@SuppressWarnings("unused")
	@FXML
	private void showFontDialog()
	{
		FontChooser fontChooser = new FontChooser();
		fontChooser.setInitFont(font);
		font = fontChooser.showChooseDialog(window);
		if(font == null)
			return;
		//update preview font label
		fontName = font.getName();
		fontSize = (int) font.getSize();
		fontL.setText(fontName + ", " + fontSize);
		fontL.setFont(font);
	}
	
	//--------------------------------------------
	@SuppressWarnings("unused")
	@FXML
	private void screenUp(ActionEvent event)
	{
		String text = previewer.getPrevScreen();
		exampleL.setText(text);
	}
	
	//--------------------------------------------
	@SuppressWarnings("unused")
	@FXML
	private void screenDown(ActionEvent event)
	{
		String text = previewer.getNextScreen();
		exampleL.setText(text);
	}
	
	//--------------------------------------------	
	@Override
	public void initialize(URL paramURL, ResourceBundle paramResourceBundle)
	{
		mainBundle = paramResourceBundle;
		
		//get version information
		String version = "";
		String className = getClass().getSimpleName() + ".class";
		String classPath = getClass().getResource(className).toString();
		String manifestPath = classPath.substring(0, classPath.indexOf('!') + 1) + "/META-INF/MANIFEST.MF";
		try
		{
			Manifest manifest = new Manifest(new URL(manifestPath).openStream());
			Attributes attributes = manifest.getMainAttributes();
			version = attributes.getValue("Display-Version");
		} catch (Exception ex)
		{
			//nothing
		}
		versionL.setText("\n\n" + version);
	
		piShadow.setVisible(false);
		progressIndicator.setVisible(false);
		progressIndicator.setProgress(-1.0);
		
		fileName.setEditable(false);
		
		profilesCB.setItems(profilesList);
		profilesCB.getSelectionModel().selectedItemProperty()
			.addListener(new ChangeListener<Profile>()
			{
		
				@Override
				public void changed(ObservableValue<? extends Profile> observable,
						Profile oldValue, Profile newValue)
				{
					int width = newValue.getWidth();
					int height = newValue.getHeight();
					profileName = newValue.getName();
					exampleL.setPrefSize(width, height);
					previewSP.setPrefSize(width+1, height+2);
					previewer.setHeight(height);
					updatePreviewText();
				}
			});
		if(profileName != null && profileName.length() > 0)
		{
			Iterator<Profile> iterator = profilesList.iterator();
			while(iterator.hasNext())
			{
				Profile p = iterator.next();
				if(p.getName().equals(profileName))
				{
					profilesCB.getSelectionModel().select(p);
					break;
				}
			}
		}
		if(profilesCB.getSelectionModel().getSelectedItem() == null)
		{
			profilesCB.getSelectionModel().select(0);
			profileName = profilesCB.getSelectionModel().getSelectedItem().getName();
		}
		
		//change choicebox list after edit profiles
		profilesController.getProfilesList().addListener(new ListChangeListener<Profile>()
		{
			@Override
			public void onChanged(
					javafx.collections.ListChangeListener.Change<? extends Profile> change)
			{
				while (change.next())
				{
					profilesList.addAll(change.getAddedSubList());
					profilesList.removeAll(change.getRemoved());
				}
			}

		});
		
		charsetNameCB.getItems().addAll(Charset.availableCharsets().keySet());
		charsetNameCB.getSelectionModel().select(charsetName);
		charsetNameCB.getSelectionModel().selectedItemProperty()
			.addListener(new ChangeListener<String>()
		{
			@Override
			public void changed(ObservableValue<? extends String> arg0, String oldValue,
					String newValue)
			{
				charsetName = newValue;
				updatePreviewText();
			}
		});
		
		fontL.fontProperty().addListener(new ChangeListener<Font>()
				{
					@Override
					public void changed(
							ObservableValue<? extends Font> observable,
							Font oldValue, Font newValue)
					{
						exampleL.setFont(font);
						previewer.setFont(font);
						updatePreviewText();
					}
				});
		
		if(bgColor == null)
			bgColor = "#000000";
		
		backgroundCP.valueProperty().addListener(new ChangeListener<Paint>()
		{

			@Override
			public void changed(
					ObservableValue<? extends Paint> paramObservableValue,
					Paint oldValue, Paint newValue)
			{
				bgColor = "#" + newValue.toString().substring(2);
				fontL.setStyle("-fx-background-color: " + bgColor);
				exampleL.setStyle("-fx-background-color: " + bgColor);
			}
		});
		backgroundCP.setValue(Color.web(bgColor));
		
		if(textColor == null)
			textColor = "#777777";
		textCP.valueProperty().addListener(new ChangeListener<Paint>()
		{
			@Override
			public void changed(
					ObservableValue<? extends Paint> paramObservableValue,
					Paint oldValue, Paint newValue)
			{
				textColor = "#" + newValue.toString().substring(2);
				fontL.setTextFill(newValue);
				exampleL.setTextFill(newValue);
			}
		});
				
		textCP.setValue(Color.web(textColor));
		
		fontL.setText(fontName + ", " + fontSize);
		fontL.setFont(font);
		exampleL.setFont(font);
		
		syslineCB.getItems().addAll(mainBundle.getString("top"), mainBundle.getString("bottom"));
		syslineCB.getSelectionModel().select(sysline);
		syslineCB.getSelectionModel().selectedIndexProperty()
			.addListener(new ChangeListener<Number>()
		{
			@Override
			public void changed(
					ObservableValue<? extends Number> paramObservableValue,
					Number oldValue, Number newValue)
			{
				sysline = (Integer) newValue;
			}
		});
		
		exampleL.setAlignment(Pos.TOP_LEFT);
		exampleL.setStyle("-fx-padding: 0 -3 0 -3;");
	}
	
	//--------------------------------------------
	private void updatePreviewText()
	{
		if(profilesCB.getSelectionModel().getSelectedItem() == null)
			return;
		try
		{
			int width = profilesCB.getSelectionModel().getSelectedItem().getWidth();
			byte[] textBytes = Generator.getText(selectedFile
					, charsetName, font, width, 2000);
			previewer.setText(textBytes);
			String screenText = previewer.getScreen(0);
			exampleL.setText(screenText);
		} catch (UnsupportedEncodingException e)
		{
			new MessageBox(window
					, mainBundle.getString("errorWindowTitle")
					, mainBundle.getString("errorFileEncoding") + "\n" + e.getMessage());
		} catch (FileNotFoundException e)
		{
			new MessageBox(window
					, mainBundle.getString("errorWindowTitle")
					, mainBundle.getString("errorFileNotFound") + ": " +selectedFile.getName());
		} catch (Exception e)
		{
			new MessageBox(window
					, mainBundle.getString("errorWindowTitle")
					, e.getMessage());
		}
	}
	
	//--------------------------------------------
	@SuppressWarnings("unused")
	@FXML
	private void createMidlet(ActionEvent event)
	{
		Profile profile = profilesCB.getSelectionModel().getSelectedItem();
		String bookName = textName.getText();
		if(profile == null || bookName == null || bookName.length() == 0)
		{
			new MessageBox(window
					, mainBundle.getString("errorWindowTitle")
					, mainBundle.getString("specifyAllData")
					);
			return;
		}
		
		// show save dialog
		DirectoryChooser directoryChooser = new DirectoryChooser();
		directoryChooser.setTitle(mainBundle.getString("saveDir"));
		File saveDirFile = new File(".");
		if(saveDir != null && saveDir.length() > 0)
		{
			saveDirFile = new File(saveDir);
		}
		directoryChooser.setInitialDirectory(saveDirFile);
		try
		{
			saveDirFile = directoryChooser.showDialog(window);
		} catch(IllegalArgumentException e)
		{
			saveDir = null;
			directoryChooser.setInitialDirectory(new File("."));
			saveDirFile = directoryChooser.showDialog(window);
		}
		if(saveDirFile == null)
			return;
		saveDir = saveDirFile.getAbsolutePath();
		
		Generator generator = new Generator(font
				, textCP.getValue(), backgroundCP.getValue()
				, profile
				, sysline
				, saveDirFile);
		
		task = new MidletTask();
		task.setOnFailed(new EventHandler<WorkerStateEvent>()
				{

					@Override
					public void handle(WorkerStateEvent event)
					{
						Throwable e = event.getSource().getException();
						StringWriter sw = new StringWriter();
						PrintWriter pw = new PrintWriter(sw);
						e.printStackTrace(pw);
						String errorMessage = sw.toString();
						new MessageBox(window, mainBundle.getString("errorWindowTitle")
								, errorMessage);
					}
				});
		
		piShadow.visibleProperty().bind(task.runningProperty());
		progressIndicator.visibleProperty().bind(task.runningProperty());
		
		task.setBookFile(selectedFile);
		task.setBookName(bookName);
		task.setCharsetName(charsetName);
		task.setGenerator(generator);
		
		new Thread(task).start();
		
		saveConfig();
	}
	
	//--------------------------------------------
	protected void saveConfig()
	{
		
		try
		{
			configIni.remove("default");
			Section section = configIni.add("default");
			section.add("sysline", sysline);
			section.add("openDir", openDir);
			section.add("saveDir", saveDir);
			section.add("profile", profileName);
			section.add("fontName", fontName);
			section.add("fontSize", fontSize);
			section.add("textColor", textColor);
			section.add("bgColor", bgColor);
			configIni.store();
		} catch (Exception e)
		{
			new MessageBox(window, mainBundle.getString("errorWindowTitle")
					, mainBundle.getString("errorSaveSettings") + "\n"
					+ e.getMessage());
		}
	}
	
	/*
	 * Task for generate midlet
	 */
	
	private class MidletTask extends Task<String>
	{
	    File bookFile;
	    String bookName;
	    String charsetName;
	    Generator generator;
	    
	    public void setGenerator(Generator generator)
	    {
	    	this.generator = generator;
	    }
	    
		public void setBookFile(File bookFile)
	    {
	    	this.bookFile = bookFile;
	    }
		
		public void setBookName(String bookName)
	    {
	    	this.bookName = bookName;
	    }
		
		public void setCharsetName(String charsetName)
		{
			this.charsetName = charsetName;
		}
		
		@Override protected String call() throws Exception
	    {
	    	
	    	generator.create(bookFile, bookName, charsetName);
			return null;
	    }
	};
}
