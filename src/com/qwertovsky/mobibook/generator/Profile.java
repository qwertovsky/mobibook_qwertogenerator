package com.qwertovsky.mobibook.generator;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Profile
{
	private String nameString = "";
	private StringProperty name;

	public StringProperty nameProperty()
	{
		if (name == null)
		{
			name = new SimpleStringProperty();
			name.set(nameString);
		}
		return name;
	}

	// --------------------------------------------
	private int width;
	private int height;
	private int lSoft = -6;
	private int rSoft = -7;
	private int menu = 0;
	private int onUp = 2;
	private int onDown = 5;
	private int onLeft = 0;
	private int onRight = 3;

	// --------------------------------------------
	public String toString()
	{
		return getName();
	}
	
	// --------------------------------------------
	public String getName()
	{
		if (name == null)
			return nameString;
		else
			return name.get();
	}

	public void setName(String name)
	{
		if (this.name != null)
			this.name.set(name);
		nameString = name;
	}

	public int getWidth()
	{
		return width;
	}

	public void setWidth(int width)
	{
		this.width = width;
	}

	public int getHeight()
	{
		return height;
	}

	public void setHeight(int height)
	{
		this.height = height;
	}

	public int getlSoft()
	{
		return lSoft;
	}

	public void setlSoft(int lSoft)
	{
		this.lSoft = lSoft;
	}

	public int getrSoft()
	{
		return rSoft;
	}

	public void setrSoft(int rSoft)
	{
		this.rSoft = rSoft;
	}

	public int getMenu()
	{
		return menu;
	}

	public void setMenu(int menu)
	{
		this.menu = menu;
	}

	public int getOnUp()
	{
		return onUp;
	}

	public void setOnUp(int onUp)
	{
		this.onUp = onUp;
	}

	public int getOnDown()
	{
		return onDown;
	}

	public void setOnDown(int onDown)
	{
		this.onDown = onDown;
	}

	public int getOnLeft()
	{
		return onLeft;
	}

	public void setOnLeft(int onLeft)
	{
		this.onLeft = onLeft;
	}

	public int getOnRight()
	{
		return onRight;
	}

	public void setOnRight(int onRight)
	{
		this.onRight = onRight;
	}

}
