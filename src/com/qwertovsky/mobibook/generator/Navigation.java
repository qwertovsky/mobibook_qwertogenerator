package com.qwertovsky.mobibook.generator;

import java.util.Locale;
import java.util.ResourceBundle;

public enum Navigation
{
	UP_LINE(0, "prevLine")
	, UP_HALF_SCREEN(1, "prevHalfScreen")
	, UP_SCREEN(2, "prevScreen")
	, DOWN_LINE(3, "nextLine")
	, DOWN_HALF_SCREEN(4, "nextHalfScreen")
	, DOWN_SCREEN(5, "nextScreen");

	private int key;
	private String name;

	Navigation(int key, String name)
	{
		ResourceBundle bundle = null;
		try
		{
			bundle = ResourceBundle.getBundle("ProfilesWindow");
		} catch(Exception e)
		{
			Locale locale = new Locale("ru","RU");
			bundle = ResourceBundle.getBundle("ProfilesWindow", locale);
		}
		this.key = key;
		if(bundle != null)
			this.name = bundle.getString(name);
		else
			this.name = name;
	}

	public String toString()
	{
		return name;
	}
	public static Navigation valueOf(int key)
	{
		Navigation[] ns = values();
		for(int i = 0; i < ns.length; i++)
		{
			if(ns[i].key == key)
				return ns[i];
		}
		return null;
	}

	public int getKey()
	{
		return key;
	}
}
